# (cons 'scheme 'variant-types)

This is an implementation of variant-types for R7RS Scheme, it's based on the tool for defining recursive data types explained in chapter 2.4 of Essentials of Programming Languages (EOPL) by Daniel P. Friedman and Mitchell Wand.

This library exports two special forms: `define-variant-type` (named in the spirit of `define-record-type`) and `variant-case`. This special forms have similar syntax to the `define-datatype` and `cases` and almost the same semantics.

## variant-type definition

```
(define-variant-type type-name type-predicate-name
  (variant-name1 (field-name1 field-predicate1)
                 ...
                 (field-nameN field-predicateN))
  ...
  (variant-nameM (field-name1 field-predicate1)
                 ...
                 (field-nameK field-predicateK)))
```

This creates a variant type, named *type-name* with some *variants*. Each variant has a variant-name and zero or more fields, each with its own field-name and associated predicate. The duplication of type names and variant names may result in problems with the execution of programs because type-name type-predicate-name and all the variant-names are bind to values.

## variant-type deconstruction

```
(variant-case type-name expression
  ((variant-name1 field-name1 ... field-nameN) code1)
  ...
  ((variant-nameM field-name1 ... field-nameK) codeM)
  (else codeElse))
```

This special form test *expression* to assert is of type *type-name* in which case deconstructs the value of the expression and evaluates to the corresponding code (which can be zero or more expressions) associated with the variant-name of the expression. The names of the fields are bounded in the evaluation of the code to the corresponding fields of the variant.
